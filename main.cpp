#include <iostream>

using namespace std;
class Base {
public:
    Base() {
        cout << "Base()\n";
    }
    virtual ~Base() {
        cout << "~Base()\n";
    }
    virtual int size() const;
    virtual void setSize(int size);

private:
    int m_size = 5;
};

class Derived: virtual public Base {
public:
    Derived() {
        cout << "Derived()\n";
    }
    ~Derived() {
        cout << "~Derived()\n";
    }
    virtual int size() const;
    void setSize(int size);

private:
    int m_size = 10;
};

class Derived2 : virtual public Base {
public:
    Derived2() {
        cout << "Derived2()\n";
    }
    ~Derived2() {
        cout << "~Derived2()\n";
    }
    int size() {
        cout << " Derived2::size() ";
        return m_size;
    }

private:
    int m_size = 600;
};

class DerivedOfDerived : public Derived2, public Derived {
public:
    DerivedOfDerived() {
        cout << "DerivedOfDerived()\n";
    }
    ~DerivedOfDerived() {
        cout << "~DerivedOfDerived()\n";
    }
};

class DerivedWithDerivedBase: public Derived {
public:
    DerivedWithDerivedBase() {
        cout << "DerivedWithDerivedBase()\n";
    }
    ~DerivedWithDerivedBase() {
        cout << "~DerivedWithDerivedBase()\n";
    }
    int size() {
        cout << " DerivedWithDerivedBase::size() ";
        return m_size;
    }

private:
    int m_size = 600;
};

int main()
{
    Base* base = new Base();
    Derived *derived = new Derived();
    DerivedOfDerived *derivedOfDerived = new DerivedOfDerived();
    Base *basePolymorphed = derived;
    cout << "base->size() " << base->size() <<"\n";
    cout << "derived->size() " << derived->size() <<"\n";
    cout << "basePolymorphed->size() " << basePolymorphed->size() <<"\n";
    cout << "derivedOfDerived->size() " << derivedOfDerived->Derived::size() << "derivedOfDerived->size() " << derivedOfDerived->Derived2::size()<<"\n";
    cout << "basePolymorphed->size() " << basePolymorphed->size() <<"\n";
    Derived *derivedPolimorphed = derivedOfDerived;
    basePolymorphed = derivedOfDerived;
    cout << "testing access to derivedderived from base classes\n";
    // for some reason there is no ambiguaity. following calls Derived::size()
    cout << "basePolymorphed->size() " << basePolymorphed->size() <<"\n";
    cout << "derivedPolimorphed->size() " << derivedPolimorphed->size() <<"\n";

    cout << " Testing polymorphism of Base<->Derived<->DerivedWithDerivedBase classes\n";
    Base *basePolymorphed2 = new DerivedWithDerivedBase();
    // casting is forbidden because of virtual inheritance. base class part is a pointer, not a part of object.
    //Derived *derivedPolimorphed2 = static_cast<Derived *>(basePolymorphed2);
    Derived *derivedPolimorphed2 = new DerivedWithDerivedBase();
    cout << "basePolymorphed2->size() " << basePolymorphed2->size() <<"\n";
    cout << "derivedPolimorphed2->size() " << derivedPolimorphed2->size() <<"\n";



    delete base;
    delete derived;
    delete derivedOfDerived;
    delete basePolymorphed2;
    delete derivedPolimorphed2;
    return 0;
}


int Base::size() const
{
    cout << " Base::size() ";
    return m_size;
}

void Base::setSize(int size)
{
    m_size = size;
}

int Derived::size() const
{
    cout << " Derived::size() ";
    return m_size;
}

void Derived::setSize(int size)
{

    m_size = size;
}
